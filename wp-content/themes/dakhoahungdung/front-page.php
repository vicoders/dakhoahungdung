<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package DaKhoaHungDung
 */

get_header(); ?>


    <div class="container">
        <div class="row">
            <div class="tuvan-benhnhan">
                <?php editContent("/wp-admin/widgets.php"); ?>
                <div class="tieude-section text-uppercase">
                    Tư vấn bệnh nhân
                </div>

                <div class="chitiet-tuvan">
                    <div class="col-md-6 col-xs-12">
                        <!--                        <div class="tieude-tuvan">-->
                        <!--                            Tư vấn trực tuyến-->
                        <!--                        </div>-->
                        <!--                        <div class="chitiet-tuvan-noidung">-->
                        <!--                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto consectetur corporis-->
                        <!--                            deleniti deserunt dignissimos dolore fugit hic natus nihil nostrum numquam praesentium quod,-->
                        <!--                            sint voluptas voluptate. At consectetur dicta fugit necessitatibus, nesciunt rem sit-->
                        <!--                            voluptates!-->
                        <!--                        </div>-->

                        <?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('Tư vấn trực tuyến')) : else : ?>
                        <?php endif; ?>
                    </div>

                    <div class="col-md-6 col-xs-12">
                        <?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('Tư vấn khám chữa bệnh')) : else : ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="slide-doingu-bacsi">

            <?php editContent('/wp-admin/edit.php?category_name=slide_doi_ngu_bac_si&post_type=slide', "Sửa slide"); ?>
            <div class="tieude-doingu-bs text-uppercase">
                Đội ngũ bác sĩ
            </div>

            <div class="slide-doingubacsi">
                <?php
                $info = [
                    'post_type' => array(
                        'slide',
                    ),
                    'category_name' => 'slide_doi_ngu_bac_si',

                ];
                $slideDoinguBacSi = new WP_Query($info);
                ?>

                <?php if ($slideDoinguBacSi->have_posts()) :
                    echo '';
                    while ($slideDoinguBacSi->have_posts()) : $slideDoinguBacSi->the_post(); ?>

                        <div>
                            <?php if (has_post_thumbnail()) { ?>

                                <?php the_post_thumbnail('slidedoingubs'); ?>

                            <?php } ?>
                        </div>

                    <?php endwhile;
                    wp_reset_postdata();
                    echo '';
                else : ?>
                    <?php _e('Sorry, no posts matched your criteria.'); ?>
                <?php endif; ?>

            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="tintuc-sukien">

                <?php editContent("/wp-admin/edit.php?post_type=tintuc", "Quản lý tin tức"); ?>
                <div class="tieude-section">
                    Tin tức - Sự kiện
                </div>

                <?php
                $info = [
                    'post_type' => array(
                        'tintuc',
                    ),
                    'posts_per_page' => 1,

                ];
                $tinmoinhat = new WP_Query($info);
                ?>

                <?php if ($tinmoinhat->have_posts()) :
                    echo '';
                    while ($tinmoinhat->have_posts()) : $tinmoinhat->the_post(); ?>
                        <div class="tinchinh">
                            <div class="row">
                                <div class="col-md-3">
                                    <?php if (has_post_thumbnail()) { ?>
                                        <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                                            <!--                                        --><?php //the_post_thumbnail([400, 300]); ?>
                                            <?php the_post_thumbnail('gg'); ?>

                                        </a>
                                    <?php } else { ?>
                                        <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                                            <img width="200" height="200"
                                                 src="<?php bloginfo('template_directory'); ?>/images/default.jpg">
                                        </a>
                                    <?php } ?>
                                </div>

                                <div class="col-md-9">
                                    <div class="tieude-tinchinh text-uppercase">
                                        <a href="<?php the_permalink(); ?>">
                                            <?php the_title(); ?>
                                        </a>

                                    </div>
                                    <div class="noidung-tinchinh">
                                        <?php the_excerpt(); ?>
                                    </div>

                                    <div class="xemtiep">
                                        <a href="<?php the_permalink(); ?>"> Xem tiếp... </a>
                                    </div>
                                </div>
                            </div>


                        </div>

                    <?php endwhile;
                    wp_reset_postdata();
                    echo '';
                else : ?>
                    <?php _e('Sorry, no posts matched your criteria.'); ?>
                <?php endif; ?>


                <div class="cactinphu">
                    <div class="row">
                        <?php
                        $info = [
                            'post_type' => array(
                                'tintuc',
                            ),
                            'posts_per_page' => 4,
                            'offset' => 1
                        ];
                        $bontinkhac = new WP_Query($info);
                        ?>

                        <?php if ($bontinkhac->have_posts()) :
                            echo '';
                            while ($bontinkhac->have_posts()) : $bontinkhac->the_post(); ?>

                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-4 col-xs-12 col-sm-4 col-lg-4">
                                            <?php if (has_post_thumbnail()) { ?>
                                                <a href="<?php the_permalink(); ?>"
                                                   title="<?php the_title_attribute(); ?>">
                                                    <?php the_post_thumbnail([200, 180]); ?>
                                                </a>
                                            <?php } else { ?>
                                                <a href="<?php the_permalink(); ?>"
                                                   title="<?php the_title_attribute(); ?>">
                                                    <img width="200" height="200"
                                                         src="<?php bloginfo('template_directory'); ?>/images/default.jpg">
                                                </a>
                                            <?php } ?>
                                        </div>
                                        <div class="col-md-8 col-xs-12 col-sm-8 col-lg-8">
                                            <div class="tieudetin-phu">
                                                <a href="<?php the_permalink(); ?>">
                                                    <?php the_title(); ?>
                                                </a>
                                            </div>
                                            <div class="noidung-tinphu">
                                                <?php the_excerpt(); ?>
                                            </div>
                                            <div class="xemtiep-tinphu">
                                                <a href="<?php the_permalink(); ?>"> Xem tiếp... </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            <?php endwhile;
                            wp_reset_postdata();
                            echo '';
                        else : ?>
                            <?php _e('Sorry, no posts matched your criteria.'); ?>
                        <?php endif; ?>

                    </div>
                </div>


            </div>
        </div>
    </div>


    <div class="ykien-wrapper">

        <?php editContent("/wp-admin/edit.php?category_name=ykienbenhnhan", "Quản trị ý kiến bệnh nh"); ?>
        <div class="container">
            <div class="row">
                <div class="slide-ykien-bn">


                    <?php
                    $info = [
                        'post_type' => array(
                            'post',
                        ),
                        'category_name' => 'ykienbenhnhan',
                    ];
                    $postYkienBenhNhan = new WP_Query($info);
                    ?>

                    <?php if ($postYkienBenhNhan->have_posts()) :
                        echo '';
                        while ($postYkienBenhNhan->have_posts()) : $postYkienBenhNhan->the_post(); ?>
                            <div class="section-ykien clearfix">
                                <div class="anhnguoi-cho-ykien col-md-3">
                                    <?php if (has_post_thumbnail()) { ?>

                                        <?php the_post_thumbnail('size230x290'); ?>

                                    <?php } ?>
                                </div>
                                <div class="ykien-benhnhan col-md-9">
                                    <div class="tieude-ykienbnhan">
                                        <?php the_title(); ?>

                                    </div>
                                    <div class="noidung-ykien">
                                        <?php the_content(); ?>

                                    </div>

                                    <div class="chunhan-ykien">
                                        <?php echo get_post_meta($post->ID, 'tacgia_ykien', true);; ?>
                                    </div>

                                </div>
                            </div>
                        <?php endwhile;
                        wp_reset_postdata();
                        echo '';
                    else : ?>
                        <?php _e('Sorry, no posts matched your criteria.'); ?>
                    <?php endif; ?>

                </div>
            </div>
        </div>
    </div>


    <div class="bando row">
        <div class="bando">
            <?php editContent("/wp-admin/widgets.php", "Sửa địa chỉ bản đồ"); ?>
            <?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('Bản đồ')) : else : ?>
            <?php endif; ?>
        </div>
    </div>


<?php
get_sidebar();
get_footer();



