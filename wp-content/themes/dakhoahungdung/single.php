<?php
/*
 Template Name: My Template
 */
?>

<?php
get_header(); ?>

    <div class="row trangtintuc-wrapper">


        <div class="container">
            <div class="div-main col-md-8">

                <?php
                if (function_exists('suabai')) {
                    suabai();
                }
                ?>

                <?php
                while (have_posts()) : the_post(); ?>


                    <div class="tieude-tintuc-chitiet">
                        <a href="<?php the_permalink(); ?>"> <?php the_title(); ?> </a>
                    </div>

                    <div class="tintuc-meta">
                        <i class="fa fa-calendar" aria-hidden="true"></i>
                        <span class="span-ngay"> <?php the_time("d-m-Y"); ?> </span>
                        <span> <?php the_time("g:i"); ?> </span>
                    </div>

                    <div class="noidung-tintucchitiet">
                        <?php the_content(); ?>
                    </div>


                    <?php

//                    the_post_navigation();

                    // If comments are open or we have at least one comment, load up the comment template.
//                    if (comments_open() || get_comments_number()) :
//                        comments_template();
//                    endif;

                endwhile; // End of the loop.
                ?>

            </div>

            <div class="div-right col-md-4">

                <?php editContent("/wp-admin/widgets.php", "Sửa phần cột phải"); ?>
                <?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('Sidebar trang tin tuc')) : else : ?>
                <?php endif; ?>


            </div>
        </div>
    </div>


<?php
//get_sidebar();
get_footer();