<?php
/**
 * Template Name: Thư Viện Video
 *
 */

$paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
$args = [
    'post_type' => 'thu_vien_video',
    'posts_per_page' => 10,
    'post_status' => ['publish'],
    'paged' => $paged
];
$gallery = new WP_Query($args);
$total_pages = $gallery->max_num_pages;

get_header(); ?>
	<div class="container thuvienvideo">
		<div class="row list-thuvien">
			<div class="col-md-8 list-images top-images">
				<div class="container">
					<div class="news-list">
						<div class="heading">
							<p>Thư viện video</p>
						</div>
					</div>
					<?php if(!empty($gallery)): ?>
					<div class="row images-row">
						<?php foreach ($gallery->posts as $key_ga => $section_image): ?>
						<?php 
							$get_video = get_field('link_youtube', $section_image->ID);
							// echo "<pre>";
							// var_dump($get_video);
							// die();
							$get_date_event = get_field('ngay_su_kien', $section_image->ID);
							if(empty($get_date_event)) {
								$get_date_event = get_the_date( 'd/m/Y', $section_image->ID );
							}
						?>
							<div class="col-md-3 col-sm-3 col-xs-12" style="margin-bottom: 20px;">
								<div class="category-image">
									<?php echo $get_video;?>

								</div>
								<p class="name-anh"><?php echo get_the_title($section_image->ID); ?></p>
								<div class="date">Ngày <?php echo $get_date_event; ?></div>
							</div>
						<?php endforeach; ?>
					</div>
					<div class="row">
						<div class="col-md-12 pagination2">
							<nav class="phantrang">
								<?php do_action('custom_paginate', $paged, $total_pages); ?>
							</nav>
						</div>
					</div>
					<?php  
					else: 
						echo 'Không có dữ liệu nào !';
					endif;
					?>
				</div>
			</div>
			<div class="col-md-4 sidebar">
				<section class="main-content">
					<?php get_template_part('sidebar'); ?>
				</section>
			</div>
		</div>
	</div>
<?php get_footer(); ?>