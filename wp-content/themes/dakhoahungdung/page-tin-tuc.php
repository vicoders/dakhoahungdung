<?php
/**
 * The template for displaying page tintuc
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package DaKhoaHungDung
 */

get_header(); ?>

    <div class="row trangtintuc-wrapper">
        <div class="container">
            <div class="div-main col-md-8">
                <div class="tieude-tintuc">
                    Tin tức
                </div>
                <div class="nhieutin">
                    <?php
                    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                    $info = [
                        'post_type' => 'tintuc',
                        'posts_per_page' => 5,
                        'paged' => $paged,
                    ];
                    $tintuc = new WP_Query($info);
                    ?>

                    <?php if ($tintuc->have_posts()) :
                        echo '';
                        while ($tintuc->have_posts()) : $tintuc->the_post(); ?>

                            <div class="mottin">
                                <div class="col-md-4 anhtin-wrapper">

                                    <a href="<?php the_permalink; ?>"> <?php the_post_thumbnail('size400x300'); ?> </a>
                                </div>
                                <div class="col-md-8 noidungtin-wrapper">
                                    <div class="tieudetin-trangtt">
                                        <div class=""><a href="<?php the_permalink(); ?>"> <?php the_title(); ?> </a>
                                        </div>
                                    </div>
                                    <div class="tomtattin">
                                        <?php the_excerpt(); ?>
                                    </div>
                                    <div class="xemtieptt">
                                        <a href="<?php the_permalink(); ?>"> Xem tiếp... </a>
                                    </div>

                                </div>
                            </div>

                        <?php endwhile; ?>

                        <!-- pagination here -->
                        <?php
                        if (function_exists(custom_pagination)) {
                            custom_pagination($tintuc->max_num_pages, "", $paged);
                        }
                        ?>
                        <?php wp_reset_postdata();
                        echo '';
                    else : ?>
                        <?php _e('Sorry, no posts matched your criteria.'); ?>
                    <?php endif; ?>


                </div>
            </div>

            <div class="div-right col-md-4">
                <?php editContent("/wp-admin/widgets.php", "Sửa phần cột phải"); ?>

                <?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('Sidebar trang tin tuc')) : else : ?>
                <?php endif; ?>


            </div>
        </div>
    </div>


<?php
//get_sidebar();
get_footer();

