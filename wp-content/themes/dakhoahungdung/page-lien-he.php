<?php
/**
 * The template for displaying page tintuc
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package DaKhoaHungDung
 */

get_header(); ?>

<div class="row trangtintuc-wrapper tranglienhe">


    <div class="container">
        <div class="div-main col-md-8">


            <?php
            if (function_exists('suabai')) {
                suabai();
            }
            ?>
            <?php
            while (have_posts()) : the_post(); ?>

                <div class="tieude-tintuc-chitiet">

                    <i class="fa fa-medkit" aria-hidden="true"></i>
                    <a href="<?php the_permalink(); ?>"> Phòng khám đa khoa Hưng Dũng </a>
                </div>


                <div class="noidung-tintucchitiet">
                    <?php the_content(); ?>
                </div>

                <?php

//                    the_post_navigation();

                // If comments are open or we have at least one comment, load up the comment template.
//                    if (comments_open() || get_comments_number()) :
//                        comments_template();
//                    endif;

            endwhile; // End of the loop.
            ?>


        </div>

        <div class="col-md-1"></div>

        <div class="div-right col-md-3">
            <?php editContent("/wp-admin/widgets.php", "Sửa phần cột phải"); ?>
            <?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('Sidebar trang LIÊN HỆ')) : else : ?>
            <?php endif; ?>

        </div>
    </div>
</div>


<?php
//get_sidebar();
get_footer();


?>

